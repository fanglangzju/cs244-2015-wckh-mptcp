#!/bin/bash

# Root Check (Code Source: http://stackoverflow.com/questions/1641975/bash-fail-if-script-is-not-being-run-by-root)
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Running TCP over WiFi"
python mptcp.py
echo "Running TCP over 3G"
python mptcp.py --TCPOver3G
echo "Running MPTCP without optimizations"
python mptcp.py --mptcp
echo "Running MPTCP with optimizations"
python mptcp.py --mptcp --opti
python plot.py -o results.png
