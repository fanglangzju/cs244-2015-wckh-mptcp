#!/bin/bash

# Root Check (Code Source: http://stackoverflow.com/questions/1641975/bash-fail-if-script-is-not-being-run-by-root)
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Running TCP over WiFi"
python mptcp.py
echo "Running TCP over 3G"
python mptcp.py --TCPOver3G
echo "Running MPTCP without optimizations"
python mptcp.py --mptcp
echo "Running MPTCP with optimizations"
python mptcp.py --mptcp --opti
python plot.py -o results_W_8_3G_2.png

# Delete result text files
rm -f TCP3G*
rm -f TCPWiFi*
rm -f MPTCPNonOpti*
rm -f MPTCPOpti*

# Sensitivity analysis
echo "Running Sensitivity analysis (16Mpbs Wifi, 2Mbps 3G)"
echo "Running TCP over WiFi"
python mptcp.py --bwWiFi 16.0
echo "Running TCP over 3G"
python mptcp.py --TCPOver3G --bwWiFi 16.0
echo "Running MPTCP without optimizations"
python mptcp.py --mptcp --bwWiFi 16.0
echo "Running MPTCP with optimizations"
python mptcp.py --mptcp --opti --bwWiFi 16.0
python plot.py -o results_W_16_3G_2.png

# Delete result text files
rm -f TCP3G*
rm -f TCPWiFi*
rm -f MPTCPNonOpti*
rm -f MPTCPOpti*

echo "Running Sensitivity analysis (32Mpbs Wifi, 2Mbps 3G)"
echo "Running TCP over WiFi"
python mptcp.py --bwWiFi 32.0
echo "Running TCP over 3G"
python mptcp.py --TCPOver3G --bwWiFi 32.0
echo "Running MPTCP without optimizations"
python mptcp.py --mptcp --bwWiFi 32.0
echo "Running MPTCP with optimizations"
python mptcp.py --mptcp --opti --bwWiFi 32.0
python plot.py -o results_W_32_3G_2.png

# Delete result text files
rm -f TCP3G*
rm -f TCPWiFi*
rm -f MPTCPNonOpti*
rm -f MPTCPOpti*

echo "Running Sensitivity analysis (64Mpbs Wifi, 8Mbps 3G)"
echo "Running TCP over WiFi"
python mptcp.py --bwWiFi 64.0
echo "Running TCP over 3G"
python mptcp.py --TCPOver3G --bwWiFi 64.0
echo "Running MPTCP without optimizations"
python mptcp.py --mptcp --bwWiFi 64.0
echo "Running MPTCP with optimizations"
python mptcp.py --mptcp --opti --bwWiFi 64.0
python plot.py -o results_W_64_3G_2.png

# Delete result text files
rm -f TCP3G*
rm -f TCPWiFi*
rm -f MPTCPNonOpti*
rm -f MPTCPOpti*
