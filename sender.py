from argparse import ArgumentParser
from struct import pack
from time import sleep
import socket, sys
from monotonic import monotonic # Monotonic time to avoid issues from NTP adjustments

# Parse arguments
parser = ArgumentParser(description="Sender for MPTCP latency measurements")
parser.add_argument('--server', '-s', help="IP address of receiver", required=True)
parser.add_argument('--port', '-p', type=int, help="Port of receiver", required=True)
parser.add_argument('--size', type=int, help="Size of each packet in bytes", default=8000)
parser.add_argument('--time', '-t', type=int, help="Number of seconds to send for", default=600)
parser.add_argument('--bufsize', type=int, help="Send buffer size in KB", default=200)
parser.add_argument('--outfile', '-o', help="Name of output file", required=True)
args = parser.parse_args()

format_string = "i%ds" % (args.size-4) # 4 byte counter + padding

def main():
    try:
        f = open(args.outfile, 'w')
    except IOError:
        sys.stderr.write("Could not open output file for writing\n")
        sys.exit(1)

    try:
        s = socket.socket(socket.AF_INET)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, args.bufsize / 2 * 1000) # The kernel doubles the value set here
        s.connect((args.server, args.port))
    except socket.error:
        sys.stderr.write("Could not connect to receiver\n")
        sys.exit(1)

    print "Connected to receiver"
    sleep(1) # Wait in case receiver needs to do some initial processing

    print "Starting packet flow"
    start_time = monotonic()
    counter = 1
    while monotonic() - start_time < args.time:
        packet = pack(format_string, counter, '')
        s.send(packet)
        timestamp = monotonic()
        f.write("%d\t%f\n" % (counter, timestamp))
        counter += 1
        # sleep(1)

    print "Shutting down"
    s.close()
    f.close()

if __name__ == '__main__':
    main()
