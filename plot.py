# PDF plotting technique from http://stackoverflow.com/a/15418687

import sys

# Needed for graph generation to work without X Window
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from numpy import linspace
from scipy.stats import gaussian_kde
from argparse import ArgumentParser

x_label = "App-Delay in ms"
y_label = "PDF in %"

# Parse arguments
parser = ArgumentParser(description="PDF Plot")

parser.add_argument('--output-file', '-o',
                    dest="outputFile",
                    help="Output File Location",
                    type=str,
                    required=True)

args = parser.parse_args()

# exps is a list of (sender_file, receiver_file, label) tuples
def plot(exps, outfile):
    for exp in exps:
        sender_file, receiver_file, exp_label = exp
        print "Plotting %s" % exp_label

        try:
            sf = open(sender_file)
        except IOError:
            sys.stderr.write("Could not open sender data file %s\n" % sender_file)
            sys.exit(1)
        try:
            rf = open(receiver_file)
        except IOError:
            sys.stderr.write("Could not open receiver data file %s\n" % receiver_file)
            sys.exit(1)

        print "Reading sender data file"
        send_times = []
        for line in sf:
            _, time = line.split('\t') # Discard the counter; it's just 1, ..., n for sender
            send_times.append(float(time))

        print "Reading receiver data file"
        receive_times = [0 for x in xrange(len(send_times))]
        for line in rf:
            counter, time = line.split('\t')
            receive_times[int(counter)-1] = float(time)

        print "Computing latencies"
        latencies = []
        for i in xrange(len(send_times)):
            if receive_times[i] == 0:
                sys.stderr.write("Warning: packet %d was not logged by receiver\n" % (i+1))
                continue
            latencies.append((receive_times[i] - send_times[i])*1000) # Convert to ms

        print "Computing and plotting PDF"
        min_latency = min(latencies)
        max_latency = max(latencies)
        kde = gaussian_kde(latencies) # Compute kernel density estimate
        dist_space = linspace(0, max_latency+20, max_latency/20+1)
        plt.plot(dist_space, kde(dist_space)*100, label=exp_label)

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='upper right')
    plt.savefig(outfile)

if __name__ == '__main__':
    exps = [
        ("MPTCPOpti-sender.txt", "MPTCPOpti-receiver.txt", "MPTCP with optimizations"),
        ("MPTCPNonOpti-sender.txt", "MPTCPNonOpti-receiver.txt", "MPTCP without optimizations"),
        ("TCPWiFi-sender.txt", "TCPWiFi-receiver.txt", "TCP over WiFi"),
        ("TCP3G-sender.txt", "TCP3G-receiver.txt", "TCP over 3G")
    ]
    plot(exps, args.outputFile)
